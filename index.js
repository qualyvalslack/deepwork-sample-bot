const { App } = require("@slack/bolt");
const slackCredentials = require("./slack.json");
const app = new App({
  token: slackCredentials.token,
  socketMode: true,
  appToken: slackCredentials.appToken,
});

//listning to event app_mentioned
app.event("app_mention", async ({ event, say }) => {
  console.log(event);
  await say("HI");
});
//Sendin initial message to provide a button to open modal
app.message(async ({ client, body, message, say }) => {
  await say({
    text: "txt",
    blocks: [
      {
        type: "actions",
        elements: [
          {
            type: "button",
            text: {
              type: "plain_text",
              text: "Click Me",
              emoji: true,
            },
            value: "click_me_123",
            action_id: "actionId-0",
          },
        ],
      },
    ],
  });
});
//Modal is sent and opened  as soon as the code is triggered wit action id
app.action("actionId-0", async ({ client, body, action, ack, say }) => {
  await client.views.open({
    trigger_id: body.trigger_id,
    view: {
      type: "modal",
      callback_id: "test",
      title: {
        type: "plain_text",
        text: "My App",
        emoji: true,
      },
      submit: {
        type: "plain_text",
        text: "Submit",
        emoji: true,
      },
      close: {
        type: "plain_text",
        text: "Cancel",
        emoji: true,
      },
      blocks: [
        {
          type: "input",
          element: {
            type: "checkboxes",
            options: [
              {
                text: {
                  type: "plain_text",
                  text: "Sunday",
                  emoji: true,
                },
                value: "value-0",
              },
              {
                text: {
                  type: "plain_text",
                  text: "Monday",
                  emoji: true,
                },
                value: "value-1",
              },
              {
                text: {
                  type: "plain_text",
                  text: "Tuesday",
                  emoji: true,
                },
                value: "value-2",
              },
              {
                text: {
                  type: "plain_text",
                  text: "Wednesday",
                  emoji: true,
                },
                value: "value-3",
              },
              {
                text: {
                  type: "plain_text",
                  text: "Thursday",
                  emoji: true,
                },
                value: "value-4",
              },
              {
                text: {
                  type: "plain_text",
                  text: "Friday",
                  emoji: true,
                },
                value: "value-5",
              },
              {
                text: {
                  type: "plain_text",
                  text: "Saturday",
                  emoji: true,
                },
                value: "value-6",
              },
            ],
            action_id: "checkboxes-action",
          },
          label: {
            type: "plain_text",
            text: "Day Selector",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "static_select",
            placeholder: {
              type: "plain_text",
              text: "Select an item",
              emoji: true,
            },
            options: [
              {
                text: {
                  type: "plain_text",
                  text: "30 Mins",
                  emoji: true,
                },
                value: "value-0",
              },
              {
                text: {
                  type: "plain_text",
                  text: "1 Hrs",
                  emoji: true,
                },
                value: "value-1",
              },
              {
                text: {
                  type: "plain_text",
                  text: "1.5 Hrs",
                  emoji: true,
                },
                value: "value-2",
              },
            ],
            action_id: "static_select-action",
          },
          label: {
            type: "plain_text",
            text: "Session Length",
            emoji: true,
          },
        },
        {
          type: "divider",
        },
        {
          type: "input",
          element: {
            type: "radio_buttons",
            options: [
              {
                text: {
                  type: "plain_text",
                  text: "Autobook Off",
                  emoji: true,
                },
                value: "value-0",
              },
              {
                text: {
                  type: "plain_text",
                  text: "Autobook On",
                  emoji: true,
                },
                value: "value-1",
              },
            ],
            action_id: "radio_buttons-action",
          },
          label: {
            type: "plain_text",
            text: "Start Time",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "datepicker",
            initial_date: "1990-04-28",
            placeholder: {
              type: "plain_text",
              text: "Select a date",
              emoji: true,
            },
            action_id: "datepicker-action",
          },
          label: {
            type: "plain_text",
            text: "Date",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "timepicker",
            initial_time: "13:37",
            placeholder: {
              type: "plain_text",
              text: "Select time",
              emoji: true,
            },
            action_id: "timepicker-action",
          },
          label: {
            type: "plain_text",
            text: "Time",
            emoji: true,
          },
        },
        {
          type: "divider",
        },
        {
          type: "input",
          element: {
            type: "radio_buttons",
            options: [
              {
                text: {
                  type: "plain_text",
                  text: "Single Session",
                  emoji: true,
                },
                value: "value-0",
              },
              {
                text: {
                  type: "plain_text",
                  text: "Repeating Weekly Session",
                  emoji: true,
                },
                value: "value-1",
              },
            ],
            action_id: "radio_buttons-action",
          },
          label: {
            type: "plain_text",
            text: "Number of sessions",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "static_select",
            placeholder: {
              type: "plain_text",
              text: "Select an item",
              emoji: true,
            },
            options: [
              {
                text: {
                  type: "plain_text",
                  text: "1 Week",
                  emoji: true,
                },
                value: "value-0",
              },
              {
                text: {
                  type: "plain_text",
                  text: "10 Week",
                  emoji: true,
                },
                value: "value-1",
              },
              {
                text: {
                  type: "plain_text",
                  text: "30 Week",
                  emoji: true,
                },
                value: "value-2",
              },
            ],
            action_id: "static_select-action",
          },
          label: {
            type: "plain_text",
            text: "Label",
            emoji: true,
          },
        },
      ],
    },
  });
});
//data being collectesd by modals will be deposited into this function
app.view("test", async ({ ack, say }) => {
  await ack();
  test(); //function call to send data to google calander
});

//A start function that triggers on the bot
console.log("Listner");
(async () => {
  // Start your app
  await app.start(process.env.PORT || 3000);

  console.log("⚡️ Bolt app is running!");
})();
///////////////////Functions, credentials and api call to connect with googe calander/////////////////////////////
const fs = require("fs");
const readline = require("readline");
const { google } = require("googleapis");
const reft = require("./tokens.json");
const creduntials = require("./credentials.json");
const TOKEN_PATH = "token.json";
var tokensflag = 0;
const oauth2client = new google.auth.OAuth2(
  creduntials.web.client_id,
  creduntials.web.client_secret,
  creduntials.web.redirect_uris[0]
);
//credentials end here
///////////////////////function begins below////////////////////////////
async function test() {
  try {
    const { tokens } = await oauth2client.getToken(code[0]);
    console.log(tokens);
  } catch {
    console.log("token already generated");
  }
  oauth2client.setCredentials({
    refresh_token: reft.refresh_token,
  });
  const at = await oauth2client.getAccessToken();
  var calendar = google.calendar({ version: "v3", oauth2client });
  calendar.events.list(
    {
      auth: oauth2client,
      calendarId: "snow.jon.2022.king@gmail.com", //enter your calander id here
      alwaysIncludeEmail: false,
    },
    (err, res) => {
      if (err) return console.log("The API returned an error: " + err);
      const events = res.data.items;
      if (events.length) {
        console.log("Upcoming 10 events:");
        events.map((event, i) => {
          const start = event.start.dateTime || event.start.date;
          console.log(`${start} - ${event.summary}`);
        });
      } else {
        console.log("No upcoming events found.");
      }
    }
  );
  calendar.events.insert(
    {
      auth: oauth2client,
      calendarId: "snow.jon.2022.king@gmail.com", //enter your calander id here
      alwaysIncludeEmail: false,
      conferenceDataVersion: 0,
      maxAttendees: 10,
      sendNotifications: false,
      sendUpdates: "none",
      supportsAttachments: "false",
      resource: {
        end: {
          date: "2022-04-05",
        },
        start: {
          date: "2022-04-01",
        },
      },
    },
    (err, res) => {
      if (err) {
        console.log(err);
      }
      console.log(res.data);
    }
  );
}
//////////////////////////////ends//////////////////////////////////////
